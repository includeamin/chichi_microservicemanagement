import json
import os

import pymongo

dirpath = os.getcwd()

DEBUG = False
#mongodb://f:f@chichiapp.ir:32032,chichiapp.ir:32033,chichiapp.ir:32034/?replicaSet=rs0
# if DEBUG is not True:
#     with open(os.path.join(dirpath, "Database/databaseConfig.json")) as f:
#         configs = json.load(f)
# else:
#     configs = {
#         "MongoDbUrl": "localhost:27017",
#         "DatabaseName": "MicroserviceManagement",
#         "ServiceCollection": "Services",
#         "TrustedServices": "TrustedServices",
#         "Configs": "Configs",
#         "LogsDatabase": "Logs",
#         "RabbitMqUrl": "pyamqp://guest@localhost//",
#         "RequestLogs": "RequestLogs"
#     }
with open(os.path.join(dirpath, "Database/databaseConfig.json")) as f:
        configs = json.load(f)

mongodb = pymongo.MongoClient(configs["MongoDbUrl"])
print(mongodb)
database = mongodb[configs["DatabaseName"]]
print(database)
ServiceDb = database[configs["ServiceCollection"]]
TrustedServiceDb = database[configs["TrustedServices"]]
ConfingsDb = database[configs["Configs"]]
LogsDb = database[configs["LogsDatabase"]]
RequestLogsDb = database[configs["RequestLogs"]]
