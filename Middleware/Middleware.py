from flask import request
from functools import wraps
from Classes.Tools import Tools
Result = Tools.Result
Error = Tools.errors

def log(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        # logger.apply_async(args=[request.host, request.remote_addr, request.path])
        return f(*args, **kwargs)

    return decorated_function




def validate_request(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        return f(*args, **kwargs)

    return decorated_function





def json_body_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        if request.get_json() is None:
            return Result(False,Error("JBR"))
        return f(*args, **kwargs)

    return decorated_function
