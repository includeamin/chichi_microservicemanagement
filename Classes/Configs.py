from datetime import datetime


class Configs:
    def __init__(self):
        self.EncryptKey = None
        self.RedisUrl = None
        self.RabbitMqUrl =None
        self.MongoDbUrl = None
        self.Create_at = datetime.now()
        self.Update_at = datetime.now()


