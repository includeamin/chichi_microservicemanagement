from datetime import datetime
import json


class Tools:
    @staticmethod
    def Result(state, description,error=None,errordata=None):
        return json.dumps({
            "State": state,
            "Description": description
        })

    @staticmethod
    def dumps(data):
        return json.dumps(data, indent=4, sort_keys=True, default=str)

    @staticmethod
    def errors(code):
        errors = {"IAE": 'item already exist',
                  "INF": 'item not found',
                  "ACCD":'Access Denied',
                  "NA":"Not Allowed",
                  "DBNS":"Database must save as a secure service"}
        if errors.__contains__(code):
            return errors[code]
        else:
            return "Error code not exist"

    @staticmethod
    def logger(data,type=None):
        pass

