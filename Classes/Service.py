from datetime import datetime
from Database.DB import ServiceDb
from Classes.Tools import Tools
from flask import Blueprint
from Classes.TrustedIp import TrustedIp
from flask import request
import re
import socket
from cryptography.fernet import Fernet
import logging
from Middleware.Middleware import log

app = Blueprint('Services', __name__, template_folder='templates')

Result = Tools.Result
Dumps = Tools.dumps
Error = Tools.errors
DataBaseCollection = {"Name": None}


class Service:
    def __init__(self, name, description, url, guncorn=None, replica=None, is_secure=False, host=None, port=None,
                 is_database=False, database_string=None
                 ):
        self.Name = name
        self.Description = description
        self.Url = url
        self.DbCollections = []
        # if is_database is False:
        #     check_is_host_name = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", host)
        #     if not check_is_host_name:
        #         self.Host = socket.gethostbyname(host)
        #     else:
        #         self.Host = host
        # else:
        #     self.Host = host
        self.Port = port
        self.Create_at = datetime.now()
        self.Cluster = {"Replica": replica}
        if guncorn != "NOTFILL":
            self.GunicornConfigures = {"ThreadMode": guncorn['ThreadMode'],
                                       "ConCurrentConnections": guncorn['ConCurrentConnections'],
                                       "ThreadCounts": guncorn['ThreadCounts']
                                       }
        else:
            self.GunicornConfigures = {"ThreadMode": None,
                                       "ConCurrentConnections": None,
                                       "ThreadCounts": None
                                       }

        self.Create_at = datetime.now()
        self.Update_at = datetime.now()

        if database_string:
            key = Fernet.generate_key()
            cipher_suite = Fernet(key)
            cipher_text = cipher_suite.encrypt(str(database_string).encode())
            self.DatabaseString = cipher_text
            self.Key = key
        else:
            self.DatabaseString = None
            self.Key = None

        self.IsSecure = is_secure
        self.IsDatabase = is_database

    @staticmethod
    def help():
        return 'this is service managment class written by amin jamal' \
               'Email : aminjamal10@gmail.com'

    @staticmethod
    def add_service(name, url, description, replica=None, gunicornConnfigs=None, is_secure=False, host=None, port=None,
                    is_database=False, database_string=None):
        # try:
            if is_database and not is_secure:
                return Result(False, Error("DBNS"))
            same_service = ServiceDb.find_one({"Name": name})
            if same_service is not None:
                return Result(False, Error("IAE"))
            if replica is None:
                replica = 1
            if gunicornConnfigs is None:
                gunicornConnfigs = dict(ThreadMode='gthread', ConCurrentConnections=2000, ThreadCounts=2000)

            if str(is_secure).lower() == "false":
                is_secure = False
            else:
                is_secure = True

            obj = Service(name, description, url, gunicornConnfigs, replica, is_secure, host=host,
                          port=port, database_string=database_string)

            ServiceDb.insert_one(obj.__dict__)
            return Result(True, "D")
        # except Exception as ex:
        #     return Result(False, ex.args)

    @staticmethod
    def get_service_url_by_name(name):
        try:

            service_indb = ServiceDb.find_one({"Name": name}, {"Url": 1, "IsSecure": 1})
            if service_indb is None:
                return Result(False, Error('INF'))
            if service_indb["IsSecure"] is True:
                return Result(False, Error("NA"))

            return Result(True, Dumps(service_indb))
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def get_service_configs_by_name(name):
        try:

            service_indb = ServiceDb.find_one({"Name": name}, {"Cluster": 1, "GunicornConfigures": 1, "IsSecure": 1
                , "DatabaseString": 1, "Key": 1})
            if service_indb is None:
                return Result(False, Error("INF"))
            # if service_indb["IsSecure"]:
            # return Result(False, Error("NA"))
            # if service_indb["Key"] is not None:
            # pass
            # ServiceDb.update_one({"_id":service_indb["_id"]},{"$set":{"Key":None}})
            return Result(True, Dumps(service_indb))
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def update(name, data):
        pass

    @staticmethod
    def get_secure_service_url_by_name(name):
        try:
            pass
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def get_secure_config_by_name(name):
        pass

    @staticmethod
    def get_database_string_of_service(name):
        try:
            service_indb = ServiceDb.find_one({"Name": name}, {"DatabaseString": 1})
            if service_indb is None:
                return Result(False, Error("INF"))
            return Result(True, Dumps(service_indb))
        except Exception as ex:
            return Result(ex.args)

    @staticmethod
    def get_connected_service_ip():
        service_in_db = ServiceDb.find({"IsSecure": {"$ne": True}}, {"URL": 1})

        ips = [item["URL"][6:6] for item in service_in_db]
        for item in ips:
            pass

    @staticmethod
    def delete(name):
        try:
            ServiceDb.delete_one({"Name": name})
            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)


# ---- routes ---------

@app.route('/Services/add', methods=["POST"])
@log
def add_service():
    # if TrustedIp.allowed(request.remote_addr, "ADDSERVICE"):
        result = Service.add_service(request.form["Name"], request.form["URL"], request.form["Descriotion"],
                                     database_string=request.form["DatabaseString"])
        return result
    # else:
    #     return Result(False, Error("ACCD")), 406


@app.route("/Services/delete/<name>")
@log
def delete_service_by_name(name):
    return Service.delete(name)


@app.route("/Services/url/get/<name>")
def get_service(name):
    result = Service.get_service_url_by_name(name)
    return result


@app.route("/Services/config/get/<name>")
def get_service_config(name):
    result = Service.get_service_configs_by_name(name)
    return result


@app.route("/Services/database/<name>")
def get_database_string(name):
    logging.log(1, msg=request.remote_addr)
    result = Service.get_database_string_of_service(name)
    return result


@app.route('/test')
def test():
    logging.warning(request.headers.getlist("X-Forwarded-For"))
    logging.warning(request.remote_user)
    logging.warning(request.headers["Host"])
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
        res = request.headers.getlist("X-Forwarded-For")
        logging.warning(res)
        return Dumps(res)
    else:
        ip = request.remote_addr

    logging.log(1, ip)
    return str(ip)
