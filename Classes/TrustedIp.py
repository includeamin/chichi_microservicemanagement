from datetime import datetime
from Classes.Tools import Tools
# from Celery.tasks import app
from Database.DB import TrustedServiceDb
# from Classes.History import History
# from termcolor import colored
from flask import Flask, request
from flask import Blueprint
import requests
import logging

Result = Tools.Result
Error = Tools.errors
app = Blueprint('Roles', __name__, template_folder='templates')


class TrustedIp:
    def __init__(self, ip, name, description, role):
        self.Ip = ip
        self.Name = name or "NOTFILL"
        self.Description = description or "NOTFILL"
        self.Roles = role or []
        self.Create_at = datetime.now()
        self.Update_at = datetime.now()

    @staticmethod
    def help():
        return 'Role Management for Microservices management'

    @staticmethod
    def add(ip: str, role: str, description=None, name=None) -> str:
        try:
            role = str(role).split(',')
            ip_in_db = TrustedServiceDb.find_one({'Ip': ip}, {"_id", "Roles"})
            if ip_in_db is not None:
                roles_in_db = list(ip_in_db["Roles"])

                for item in role:
                    if not roles_in_db.__contains__(item):
                        roles_in_db.append(item)

                TrustedServiceDb.update_one({'_id': ip_in_db["_id"]},
                                            {"$set": {"Roles": roles_in_db, "Update_at": datetime.now()}})
            else:
                TrustedServiceDb.insert_one(TrustedIp(ip, name, description, role).__dict__)
            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def delete(ip: str) -> str:
        try:
            TrustedServiceDb.delete_one({'Ip': ip}, {"_id"})
            return Result(True, 'D')
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def delete_role(ip: str, rolename: str) -> str:
        try:
            role_in_db = TrustedServiceDb.find_one({"Ip": ip}, {"_id": 1})
            if role_in_db is None:
                return Result(False, Error('INF'))
            TrustedServiceDb.update_one({"_id": role_in_db["_id"]}, {"$pull": {"Roles": rolename}})
            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def allowed(ip: str, role: str) -> bool:
        try:

            # logging.warning(colored(ip))
            temp = TrustedServiceDb.find({})
            for i in temp:
                print(i)
            ip_in_db = TrustedServiceDb.find_one({'Ip': ip}, {"Roles": 1})
            print(ip_in_db)
            if ip_in_db is None:
                return False
            if list(ip_in_db["Roles"]).__contains__(role):
                return True

            return False
        except Exception as ex:
            return Result(False, ex.args)


class Trusted_Test:
    @staticmethod
    def add():
        a = TrustedIp.add("127.0.0.1", "ADDSERVICE", "admin of all system", "AminJamal")
        # print(colored("Test Ip and Role"), colored(a, 'red'))
        return Result(True, "Add IP and Role Test")

    @staticmethod
    def allowed():
        a = TrustedIp.allowed("127.0.0.1", "ADDROLE")
        # print(colored("Test Ip and Role"), colored(a, 'red'))

    @staticmethod
    def delete():
        a = TrustedIp.delete("127.0.0.1")
        # print(colored("Test Ip and Role"), colored(a, 'red'))

    @staticmethod
    def add_trusted_api():
        a = requests.post('https://chichiapp.ir:3000/Roles/add',
                          data={"Ip": "93.117.177.70", "RoleName": "ADDSERVICE,ADDROLE"}, verify=False)
        # print(colored("Test Ip and Role"), colored(a, 'red'))


# ---- Routes -------

@app.route("/Roles/help")
def help():
    return TrustedIp.help()


@app.route('/Roles/add', methods=["POST"])
def add_role():
    if TrustedIp.allowed(request.remote_addr, "ADDROLE"):
        form = request.form
        ip = form["Ip"]
        role = form["RoleName"]
        result = TrustedIp.add(ip, role)
        return result
    else:
        return Result(False, Error("ACCD")), 406


@app.route("/Roles/delete")
def delete_role():
    if TrustedIp.allowed(request.remote_addr, "DELETEROLE"):
        form = request.form
        ip = form["Ip"]
        role = form["RoleName"]
        result = TrustedIp.delete_role(ip, role)
        return result
    else:
        return Result(False, Error("ACCD")), 406


@app.route("/Roles/ip/delete")
def delete_role_ip():
    if TrustedIp.allowed(request.remote_addr, "DELETEIP"):
        form = request.form
        ip = form["Ip"]
        result = TrustedIp.delete(ip)
        return result
    else:
        return Result(False, Error("ACCD")), 406


if __name__ == '__main__':
    Trusted_Test.add_trusted_api()
    # Trusted_Test.add()
