import datetime
from Database.DB import RequestLogsDb


class Log:
    @staticmethod
    def add_log(ip, user_id, route):
        RequestLogsDb.insert_one({
            "Ip": ip,
            "UserId": user_id,
            "Route": route,
            "Create_at": datetime.datetime.now()
        })
