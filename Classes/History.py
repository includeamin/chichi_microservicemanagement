from datetime import datetime
# from Celery.tasks import app
from Classes.Tools import Tools
from Database.DB import LogsDb


Result = Tools.Result


class History:
    def __init__(self, name, ip, rawdata):
        self.Name = name
        self.Ip = ip
        self.RawData = rawdata
        self.Create_at = datetime.now()
        self.Update_at = datetime.now()

    @staticmethod
    # @app.task
    def add(name,ip,rawdata):
        try:
            name = str(name).upper()
            LogsDb.insert_one(History(name,ip,rawdata=rawdata).__dict__)
        except Exception as ex:
            LogsDb.insert_one(History(name, ip, rawdata=ex.args).__dict__)



