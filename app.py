from flask import Flask, jsonify, send_file, abort
from flask_cors import CORS
from Classes.Tools import Tools
from Classes.TrustedIp import app as TrustedServiceRoute
from Classes.Service import app as ServiecesRoute
from Database.DB import ServiceDb

Result = Tools.Result
Error = Tools.errors

app = Flask(__name__)

cors = CORS(app)


# ----- BluePrint Registerer -----
app.register_blueprint(ServiecesRoute)
app.register_blueprint(TrustedServiceRoute)


# ---- End BluePrint Registerer -----

@app.route("/")
def what():
    return jsonify({"What": "MicroServiceManagement",
                    "Author": "AminJamal",
                    "NickName": "Includeamin",
                    "Email": "aminjamal10@gmail.com",
                    "WebSite": "includeamin.com"})

@app.route('/healthz')
def check_liveness():
        service = ServiceDb.find_one({},{})
        return 'd'


@app.route('/loaderio-67a94f0721ce7a5f9062cd34ed2f1885/')
def load_test():
    return send_file('./a.txt')


if __name__ == '__main__':
    app.run(port=3000, debug=True, host='0.0.0.0', threaded=True)
