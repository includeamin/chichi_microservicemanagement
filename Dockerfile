#FROM pypy:3.6
#
#EXPOSE 3000
#
#RUN apt-get update -y && \
#    apt-get install -y python3 python3-dev python3-pip
#
#
#
## We copy just the requirements.txt first to leverage Docker cache
#COPY ./requirements.txt ./app/requirements.txt
#
#COPY . ./app
#
#RUN ls
#RUN cd /app/Configures && ls
#
#WORKDIR app
#
#
#RUN pip3  install -r requirements.txt
#RUN pip3 install gunicorn
#RUN pip3 install termcolor
##CMD ["/usr/local/bin/gunicorn", "--config", "gunicorn_config.py" , "app:app"]
#CMD ['python3','app.py']

##FROM includeamin/pypy_flask:v1
#FROM includeamin/chichi_fastapi:v1
FROM includeamin/pypy3_gunicorn:v1

EXPOSE 3000
COPY . app
WORKDIR app
RUN ls

#CMD ["python","-u","app.py"]
#CMD ["/usr/local/bin/gunicorn","--certfile","cert.crt","--keyfile","key.key", "--config", "gunicorn_config.py" , "app:app"]
CMD ["gunicorn", "--config", "gunicorn_config.py" , "app:app"]
#CMD ["/usr/local/bin/gunicorn", "--config", "gunicorn_config.py" , "app:app"]
#CMD ["/usr/local/bin/gunicorn" , "app:app","-w","4", "-k", "uvicorn.workers.UvicornWorker","--bind","0.0.0.0:3000","--log-level","debug","--threads=2"]



