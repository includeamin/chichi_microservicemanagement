import json

with open("./Config.json") as f:
    configs = json.load(f)

RabittMq = configs["RabbitMqUrl"]

Permissions = ["ADDROLE",
               "ADDSERVICE",
               "DELETEROLE",
               "DELETEIP"
               ]
